var certificatesSlider = new Swiper('.certificates-slider', {
    speed: 400,
    spaceBetween: 24,
    slidesPerView: 1,
    loop: true,
    navigation: {
        nextEl: '.certificates-slider-next',
        prevEl: '.certificates-slider-prev',
    },
    pagination: {
        el: '.certificates-slider-pagination',
        type: 'bullets',
        clickable: true,
    },
    breakpoints: {
        576: {
            slidesPerView: 2,
        },
        1200: {
            slidesPerView: 3,
        },
    },
});

var reviewsSlider = new Swiper('.main-reviews__slider', {
    speed: 400,
    spaceBetween: 40,
    slidesPerView: 1,
    loop: true,
    navigation: {
        nextEl: '.main-reviews__slider--next',
        prevEl: '.main-reviews__slider--prev',
    },

    pagination: {
        el: '.main-reviews__slider--pagination',
        type: 'fraction',
    },

    breakpoints: {
        992: {
            slidesPerView: 2,
        }
    },
});

var mainServices = new Swiper('.main-services__slider', {
    speed: 400,
    spaceBetween: 20,
    slidesPerView: 1,
    centeredSlides: true,
    loop: true,

    pagination: {
        el: '.main-services-pagination',
        type: 'fraction',
    },
    navigation: {
        nextEl: '.main-services-next',
        prevEl: '.main-services-prev',
    },
});
