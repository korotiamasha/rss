/*header search form*/
$('.js-header-search').on('click', function () {
    $('.header-search').toggleClass('show');
});

$('.header-search__close').on('click', function () {
    $('.header-search').toggleClass('show');
});

$('.header__burger').on('click', function () {
    $('.header__burger').toggleClass('open');
    $('.mobile-menu').addClass('show');
    $('body').addClass('.modal-open');
});

$('.js-call-form').on('click', function () {
    if($('.call-form').hasClass('show')) {
        $('.call-form').removeClass('show');
    } else {
        $('.call-form').addClass('show');
    }
});

$('.call-form__close').on('click', function () {
    $('.call-form').removeClass('show');
});

$('.mobile-menu .has-child > a').on('click', function (event) {
    event.preventDefault();
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).next().removeClass('open');
    } else {
        $(this).addClass('active');
        $(this).next().addClass('open');
    }
});

$('.mobile-menu__close').on('click', function () {
    $('.mobile-menu').removeClass('show');
    $('body').removeClass('.modal-open');
});

$(window).scroll(function() {
    if($(window).scrollTop() > $('.header-wrap').height()) {
        $('.header-wrap').addClass('fixed');
        $('.page').addClass('fixed-header');
    } else {
        $('.header-wrap').removeClass('fixed');
        $('.page').removeClass('fixed-header');
    }
});
