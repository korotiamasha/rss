var certificatesBigSlider = new Swiper('.certificates__slider', {
    speed: 400,
    slidesPerView: 1,
    slidesPerColumn: 1,
    spaceBetween: 30,

    navigation: {
        nextEl: '.certificates__slider-next',
        prevEl: '.certificates__slider-prev',
    },
    pagination: {
        el: '.certificates__slider-pagination',
        type: 'fraction',
    },
    breakpoints: {
        768: {
            slidesPerView: 2,
            slidesPerColumn: 2,
        },
        992: {
            slidesPerView: 3,
            slidesPerColumn: 2,
        },
        1200: {
            slidesPerView: 4,
            slidesPerColumn: 2,
        },
    },
});
