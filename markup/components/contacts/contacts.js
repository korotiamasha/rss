if ($('.contact__map').length > 0) {
    ymaps.ready(init);

    function init(){

        var myMap;

        myMap = new ymaps.Map("map-msk", {
            center: [60.010731, 30.290715],
            zoom: 17,
            controls: [],

        });

        var position = myMap.getGlobalPixelCenter();
        myMap.setGlobalPixelCenter([ position[0] - 300, position[1] ]);
        myMap.behaviors.disable('scrollZoom');


        var myPlacemark = new ymaps.Placemark([60.010731, 30.290715] , {},
            { iconLayout: 'default#image',
                iconImageHref: 'static/img/general/point.png',
                iconImageSize: [40, 40],
                iconImageOffset: [-20, -47] });

        myMap.geoObjects.add(myPlacemark);


        /*/*/
        var myGermanMap = new ymaps.Map("map-spb", {
            center: [60.010731, 30.290715],
            zoom: 17,
            controls: [],

        });

        var position = myGermanMap.getGlobalPixelCenter();
        myGermanMap.setGlobalPixelCenter([ position[0] - 300, position[1] ]);
        myGermanMap.behaviors.disable('scrollZoom');

        var myGermanyPlacemark = new ymaps.Placemark([60.010731, 30.290715] , {},
            { iconLayout: 'default#image',
                iconImageHref: 'static/img/general/point.png',
                iconImageSize: [40, 40],
                iconImageOffset: [-20, -47] });

        myGermanMap.geoObjects.add(myGermanyPlacemark);

    }

}
